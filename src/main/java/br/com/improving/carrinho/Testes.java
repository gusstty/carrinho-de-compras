/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.improving.carrinho;

import java.math.BigDecimal;

/**
 *
 * @author Gustavo
 */
public class Testes {
	public static void main(String[] args){
		
	    CarrinhoComprasFactory carrinhoComprasFactory = new CarrinhoComprasFactory();
	   
		//1º CLIENTE
		CarrinhoCompras carrinho2 = carrinhoComprasFactory.criar("Gustavo");
		Produto produto2 = new Produto(1L, "Feijao");
		carrinho2.adicionarItem(produto2, new BigDecimal(3.50), 30);
		System.out.println("Valor do Carrinho 2: "+carrinho2.getValorTotal());
		
		
		Produto produto3 = new Produto(2L, "Carne");
		carrinho2.adicionarItem(produto3, new BigDecimal(13.50), 5);
		System.out.println("Valor do Carrinho 2: "+carrinho2.getValorTotal());
		
		Produto produto4 = new Produto(3L, "Melancia");
		carrinho2.adicionarItem(produto4, new BigDecimal(8.00), 2);
		System.out.println("Valor do Carrinho 2: "+carrinho2.getValorTotal());
		
		Produto produto5 = new Produto(4L, "Suco Del Valle");
		carrinho2.adicionarItem(produto5, new BigDecimal(4.00), 6);
		System.out.println("Valor do Carrinho 2: "+carrinho2.getValorTotal());
		
		
		BigDecimal valorTicketMedio1 = carrinhoComprasFactory.getValorTicketMedio();
		System.out.println("Valor do Ticket Medio1:  "+valorTicketMedio1);
		
		
		//2º CLIENTE
		CarrinhoCompras carrinho3 = carrinhoComprasFactory.criar("Luiz");
		Produto produto6 = new Produto(1L, "Feijao");
		carrinho3.adicionarItem(produto6, new BigDecimal(7.50), 33);
		System.out.println("Valor do Carrinho 3: "+carrinho3.getValorTotal());
		
		Produto produto7 = new Produto(2L, "Carne");
		carrinho3.adicionarItem(produto7, new BigDecimal(18.50), 51);
		System.out.println("Valor do Carrinho 3: "+carrinho3.getValorTotal());
		
		Produto produto8 = new Produto(3L, "Melancia");
		carrinho3.adicionarItem(produto8, new BigDecimal(80.00), 22);
		System.out.println("Valor do Carrinho 3: "+carrinho3.getValorTotal());
		
		Produto produto9 = new Produto(4L, "Suco Del Valle");
		carrinho3.adicionarItem(produto9, new BigDecimal(40.00), 12);
		System.out.println("Valor do Carrinho 3: "+carrinho3.getValorTotal());
		
		
		Produto produto10 = new Produto(5L, "Vinagre");
		carrinho3.adicionarItem(produto10, new BigDecimal(32.00), 7);
		System.out.println("Valor do Carrinho 3: "+carrinho3.getValorTotal());
		
		boolean removerItem = carrinho3.removerItem(produto10);
		if(removerItem){
			System.out.println("O produto "+produto10.getDescricao()+ " foi removido com sucesso");
		}

		
		//RESULTADOS  VALOR DE TODOS OS CARRINHOS / PELA QUANTIDADE DE CARRINHOS
		// 212.5 + 3431 = 3.643,45 /2 = 1821,75 arredondado para 1822
		BigDecimal valorTicketMedio2 = carrinhoComprasFactory.getValorTicketMedio();
		System.out.println("Valor do Ticket Medio2:  "+valorTicketMedio2);
		
    }

	
}
