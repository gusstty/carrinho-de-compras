package br.com.improving.carrinho;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {
	
	ArrayList<Item> itensCarrinho = new ArrayList<>();
    /**
     * Permite a adição de um novo item no carrinho de compras.
     *
     * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser seguidas:
     * - A quantidade do item deverá ser a soma da quantidade atual com a quantidade passada como parâmetro.
     * - Se o valor unitário informado for diferente do valor unitário atual do item, o novo valor unitário do item deverá ser
     * o passado como parâmetro.
     *
     * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao carrinho de compras.
     *
     * @param produto
     * @param valorUnitario
     * @param quantidade
     */
    public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade){
		
		Item item = new Item(produto, valorUnitario, quantidade);

		if (itensCarrinho.size() > 0) {

			//Faz um loop por todos os produtos do carrinho
			for (Item item_carrinho : itensCarrinho) {

				//Verifica se um produto já está no carrinho
				if (item_carrinho.getProduto().equals(produto)) {

					Integer novaQtde = (item_carrinho.getQuantidade() + quantidade);
					item.setQuantidade(novaQtde);

					if (item_carrinho.getValorUnitario() == valorUnitario) {
						item.setValorUnitario(valorUnitario);
					}

					if (!itensCarrinho.add(item)) {
						throw new RuntimeException(
								"Não foi possível incluir o item no carrinho de compras!");
					}
					// Pausa o for para não perder tempo percorrendo os outros itens.
					break;
				}

			}
			// Adiciona o item caso não tenha outro produto com o mesmo nome
			if (!itensCarrinho.add(item)) {
				throw new RuntimeException("Não foi possível incluir o item no carrinho de compras!");
			}
			// Caso o carrinho esteja vazio.
		} else {
			if (!itensCarrinho.add(item)) {
				throw new RuntimeException("Não foi possível incluir o item no carrinho de compras!");
			}
		}
	}

    /**
     * Permite a remoção do item que representa este produto do carrinho de compras.
     *
     * @param produto
     * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
     * caso o produto não exista no carrinho.
     */
    public boolean removerItem(Produto produto) {
		
		if (itensCarrinho.size() > 0) {

			for (Item item : itensCarrinho) {
				if (item.getProduto().equals(produto)) {
					itensCarrinho.remove(item);
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}
    /**
     * Permite a remoção do item de acordo com a posição.
     * Essa posição deve ser determinada pela ordem de inclusão do produto na 
     * coleção, em que zero representa o primeiro item.
     *
     * @param posicaoItem
     * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
     * caso o produto não exista no carrinho.
     */
   public boolean removerItem(int posicaoItem) {
		if (itensCarrinho.size() > 0) {
			try {
				itensCarrinho.remove(posicaoItem);
			} catch (IndexOutOfBoundsException exception) {
				return false;
			}
			return true;
		} else {
			return false;
		}

	}
    
    /**
     * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais
     * de todos os itens que compõem o carrinho.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorTotal() {
		BigDecimal valorTotal = new BigDecimal(0);

		for (Item itens: itensCarrinho ){
			valorTotal = valorTotal.add(itens.getValorTotal());
		}
		return valorTotal;
    }

    /**
     * Retorna a lista de itens do carrinho de compras.
     *
     * @return itens
     */
   public Collection<Item> getItens() {

		return itensCarrinho;

	}

	public CarrinhoCompras() {
	}
	
	
}