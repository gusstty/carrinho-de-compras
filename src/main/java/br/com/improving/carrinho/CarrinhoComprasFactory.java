package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {

	private List<Cliente> listaCliente = new ArrayList<Cliente>();
	
    /**
     * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
     *
     * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho deverá ser retornado.
     *
     * @param identificacaoCliente
     * @return CarrinhoCompras
     */
	
    public CarrinhoCompras criar(String identificacaoCliente) {
		Cliente cliente = new Cliente(identificacaoCliente);
		
		if(listaCliente.size() > 0){
			
			//Retorna o carrinho de um cliente que já está na lista
			//A para saber se uma lista contem um objeto o java o compara usando o método equals
			//ao sobrescrever o equals da classe Cliente apenas com nome obtem-se o resultado desejado.
			if(listaCliente.contains(cliente)){
				int posicao = listaCliente.indexOf(cliente);
				return listaCliente.get(posicao).getCarrinhoCompras();
			}else{
				//Cria um novo carrinho, cliente novo.
				cliente.setCarrinhoCompras(new CarrinhoCompras());
				listaCliente.add(cliente);
				return cliente.getCarrinhoCompras();
			}		
		}else{
			//Não existem clientes cadastrados, então cria um novo carrinho e o cadastra ao cliente, retornado ao final.
			cliente.setCarrinhoCompras(new CarrinhoCompras());
			listaCliente.add(cliente);
			return cliente.getCarrinhoCompras();
		}				
		
    }

    /**
     * Retorna o valor do ticket médio no momento da chamada ao método.
     * O valor do ticket médio é a soma do valor total de todos os carrinhos de compra dividido
     * pela quantidade de carrinhos de compra.
     * O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
     * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorTicketMedio() {
		BigDecimal valorTicketMedio = new BigDecimal(0);
		
		int qtdeCarrinhos = listaCliente.size();
		BigDecimal valor_total_carrinhos = new BigDecimal(0);
		//Pegando e somando o valor de todos os carrinhos
		for(Cliente cliente: listaCliente){
			valor_total_carrinhos = valor_total_carrinhos.add(cliente.getCarrinhoCompras().getValorTotal());
		}
		BigDecimal divisao = valor_total_carrinhos.divide(new BigDecimal(qtdeCarrinhos));
		
		//Aqui ele remove as 2 decimais e arredonda para cima 5 6 7 8 9 e 4 3 2 1
		BigDecimal valor_final = divisao.setScale(0, RoundingMode.HALF_EVEN);
		
		return valorTicketMedio.add(valor_final);
    }

    /**
     * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar.
     * Deve ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos de compras.
     *
     * @param identificacaoCliente
     * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um carrinho de compras e
     * e false caso o cliente não possua um carrinho.
     */
    public boolean invalidar(String identificacaoCliente) {
		Cliente cliente = new Cliente(identificacaoCliente);		
		
			if(listaCliente.contains(cliente)){
				int posicao = listaCliente.indexOf(cliente);
				listaCliente.remove(posicao);
				return true;
			}
			
		return false;	
    }
}
